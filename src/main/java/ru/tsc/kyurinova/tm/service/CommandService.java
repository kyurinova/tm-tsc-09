package ru.tsc.kyurinova.tm.service;

import ru.tsc.kyurinova.tm.api.ICommandRepository;
import ru.tsc.kyurinova.tm.api.ICommandService;
import ru.tsc.kyurinova.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
